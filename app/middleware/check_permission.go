package middleware

import (
	"easygoadmin/app/service"
	"easygoadmin/utils"
	"easygoadmin/utils/common"
	"fmt"
	"github.com/kataras/iris/v12"
	"strings"
)

// 鉴权中间件
func CheckPermission(ctx iris.Context) {
	fmt.Println("鉴权中间件")

	// 用户ID
	userId := utils.Uid(ctx)
	if userId > 0 {
		if userId == 1 {
			// 超级管理员，拥有全部权限(用户ID=1)
			// 此时不用做任何处理，直接放行
		} else {
			// 非超级管理员(用户ID>1)
			// 请求地址
			requestURL := ctx.Path()
			// 白名单URL
			ignoreURL := []string{"/captcha", "/login", "/logout", "/index", "/main", "/"}
			if !utils.InStringArray(requestURL, ignoreURL) &&
				!strings.Contains(requestURL, "/static") {
				// 请求地址解析
				item := strings.Split(requestURL, "/")
				if len(item) < 3 {
					ctx.JSON(common.JsonResult{
						Code: -1,
						Msg:  "权限不足",
					})
					return
				}
				// 模块名称
				moduleName := item[1]
				// 节点名称
				activeName := item[2]
				if activeName == "edit" {
					// 编辑表单验证详情节点(此处主要纠正请求地址与节点标识匹配)
					activeName = "detail"
				} else if activeName == "setStatus" {
					// 状态设置验证(此处主要纠正请求地址与节点标识匹配)
					activeName = "status"
				}
				// 权限节点
				permission := "sys:" + moduleName + ":" + activeName
				// 获取当前用户权限节点列表
				permissionList := service.Menu.GetPermissionsList(userId)
				// 鉴权验证主要验证操作节点权限，模块列表主页不做强制验证
				if !utils.InStringArray(permission, permissionList) && activeName != "index" {
					ctx.JSON(common.JsonResult{
						Code: -1,
						Msg:  "权限不足",
					})
					return
				}
			}
		}
	}

	// 前置中间件
	ctx.Application().Logger().Infof("Runs before %s", ctx.Path())
	ctx.Next()
}
