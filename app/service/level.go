// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package service

import (
	"easygoadmin/app/dto"
	"easygoadmin/app/model"
	"easygoadmin/conf"
	"easygoadmin/utils"
	"easygoadmin/utils/gconv"
	"errors"
	"fmt"
	"github.com/xuri/excelize/v2"
	"strconv"
	"strings"
	"time"
)

var Level = new(levelService)

type levelService struct{}

func (s *levelService) GetList(req dto.LevelPageReq) ([]model.Level, int64, error) {
	// 初始化查询实例
	query := utils.Engine.Where("mark=1")
	// 字典名称查询
	if req.Name != "" {
		query = query.Where("name like ?", "%"+req.Name+"%")
	}
	// 排序
	query = query.Asc("sort")
	// 分页设置
	offset := (req.Page - 1) * req.Limit
	query = query.Limit(req.Limit, offset)
	// 查询列表
	list := make([]model.Level, 0)
	count, err := query.FindAndCount(&list)
	// 返回结果
	return list, count, err
}

func (s *levelService) Add(req dto.LevelAddReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 实例化对象
	var entity model.Level
	entity.Name = req.Name
	entity.Status = gconv.Int(req.Status)
	entity.Sort = gconv.Int(req.Sort)
	entity.CreateUser = userId
	entity.CreateTime = time.Now().Unix()
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	entity.Mark = 1
	// 插入数据
	return entity.Insert()
}

func (s *levelService) Update(req dto.LevelUpdateReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 查询记录
	entity := &model.Level{Id: gconv.Int(req.Id)}
	has, err := entity.Get()
	if err != nil || !has {
		return 0, errors.New("记录不存在")
	}
	entity.Name = req.Name
	entity.Status = gconv.Int(req.Status)
	entity.Sort = gconv.Int(req.Sort)
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	// 更新记录
	return entity.Update()
}

func (s *levelService) Delete(ids string) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 记录ID
	idsArr := strings.Split(ids, ",")
	if len(idsArr) == 1 {
		// 单个删除
		entity := &model.Level{Id: gconv.Int(ids)}
		rows, err := entity.Delete()
		if err != nil || rows == 0 {
			return 0, errors.New("删除失败")
		}
		return rows, nil
	} else {
		// 批量删除
		count := 0
		for _, v := range idsArr {
			id, _ := strconv.Atoi(v)
			entity := &model.Level{Id: id}
			rows, err := entity.Delete()
			if rows == 0 || err != nil {
				continue
			}
			count++
		}
		return int64(count), nil
	}
}

func (s *levelService) Status(req dto.LevelStatusReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 查询记录是否存在
	info := &model.Level{Id: gconv.Int(req.Id)}
	has, err := info.Get()
	if err != nil || !has {
		return 0, errors.New("记录不存在")
	}

	// 设置状态
	entity := &model.Level{}
	entity.Id = info.Id
	entity.Status = gconv.Int(req.Status)
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	return entity.Update()
}

func (s *levelService) ImportExcel(fileURL string, userId int) (int, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 获取本地文件绝对地址
	filePath := conf.CONFIG.Attachment.FilePath + fileURL
	// 读取Excel文件
	file, err := excelize.OpenFile(filePath)
	if err != nil {
		return 0, errors.New("excel文件读取失败")
	}
	// 读取第一张Sheet表
	rows, err := file.Rows("Sheet1")
	if err != nil {
		return 0, errors.New("excel文件读取失败")
	}
	// 计数器
	totalNum := 0
	// Excel文件头，此处必须与Excel模板头保持一致
	excelHeader := []string{"职级名称", "职级状态", "显示顺序"}
	// 循环遍历读取的数据源
	for rows.Next() {
		// Excel列对象
		item, err2 := rows.Columns()
		if err2 != nil {
			return 0, errors.New("excel文件解析异常")
		}
		// 读取的列数与Excel头列数不等则跳过读取下一条
		if len(item) != len(excelHeader) {
			continue
		}
		// 如果是标题栏则跳过
		if item[1] == "职级状态" {
			continue
		}
		// 职级名称
		name := item[0]
		// 职级状态
		status := 1
		if item[1] == "正常" {
			status = 1
		} else {
			status = 2
		}
		// 显示顺序
		sort, _ := strconv.Atoi(item[2])
		// 实例化职级导入对象
		level := model.Level{
			Name:       name,
			Status:     status,
			Sort:       sort,
			CreateUser: userId,
			CreateTime: time.Now().Unix(),
			UpdateUser: userId,
			UpdateTime: time.Now().Unix(),
			Mark:       1,
		}
		// 插入职级数据
		if _, err := level.Insert(); err != nil {
			return 0, err
		}
		// 计数器+1
		totalNum++
	}
	return totalNum, nil
}

func (s *levelService) GetExcelList(req dto.LevelPageReq) (string, error) {
	// 初始化查询实例
	query := utils.Engine.Where("mark=1")
	// 字典名称查询
	if req.Name != "" {
		query = query.Where("name like ?", "%"+req.Name+"%")
	}
	// 排序
	query = query.Asc("sort")
	// 查询列表
	list := make([]model.Level, 0)
	// 查询数据
	err := query.Find(&list)
	if err != nil {
		return "", errors.New("数据查询失败")
	}

	// 循环遍历处理数据源
	excel := excelize.NewFile()
	excel.SetSheetRow("Sheet1", "A1", &[]string{"ID", "职级名称", "职级状态", "排序", "创建时间"})
	for i, v := range list {
		axis := fmt.Sprintf("A%d", i+2)
		excel.SetSheetRow("Sheet1", axis, &[]interface{}{
			v.Id,
			v.Name,
			v.Status,
			v.Sort,
			time.Unix(v.CreateTime, 0).Format("2006-01-02 15:04:05"),
		})
	}
	// 定义文件名称
	fileName := fmt.Sprintf("%s.xlsx", time.Now().Format("20060102150405"))
	// 设置Excel保存路径
	filePath := fmt.Sprintf("%s/temp/%s", conf.CONFIG.Attachment.FilePath, fileName)
	err2 := excel.SaveAs(filePath)
	// 获取文件URL地址
	fileURL := utils.GetImageUrl(strings.ReplaceAll(filePath, conf.CONFIG.Attachment.FilePath, ""))
	// 返回结果
	return fileURL, err2
}
