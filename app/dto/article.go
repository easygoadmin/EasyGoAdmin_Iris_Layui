// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 文章Dto
 * @author 半城风雨
 * @since 2023-08-29
 * @File : article
 */
package dto

import "github.com/gookit/validate"

// 分页查询
type ArticlePageReq struct {
	Page   int    `form:"page"`   // 页码
	Limit  int    `form:"limit"`  // 每页数
	Title  string `form:"title"`  // 文章标题
	IsTop  int    `form:"isTop"`  // 是否置顶：1已置顶 2未置顶
	Status int    `form:"status"` // 状态：1显示 2隐藏
}

// 添加文章
type ArticleAddReq struct {
	Id         int    `form:"id"`
	Title      string `form:"title" validate:"required"`      // 文章标题
	Cover      string `form:"cover" validate:"required"`      // 文章封面
	Tags       string `form:"tags" validate:"required"`       // 文章标签
	ItemId     int    `form:"itemId" validate:"int"`          // 站点ID
	CateId     int    `form:"cateId" validate:"int"`          // 栏目ID
	IsTop      int    `form:"isTop" validate:"int"`           // 是否置顶：1已置顶 2未置顶
	ViewNum    int    `form:"viewNum" validate:"int"`         // 浏览次数
	Author     string `form:"author" validate:"required"`     // 作者
	SourceName string `form:"sourceName" validate:"required"` // 来源名称
	SourceUrl  string `form:"sourceUrl" validate:"required"`  // 来源链接
	Guide      string `form:"guide" validate:"required"`      // 文章导读
	Imgs       string `form:"imgs" validate:"required"`       // 图集
	Content    string `form:"content" validate:"required"`    // 文章内容
	Status     int    `form:"status" validate:"int"`          // 状态：1显示 2隐藏
	File       string `form:"file"`                           // 文件
}

// 添加表单验证
func (v ArticleAddReq) Messages() map[string]string {
	return validate.MS{
		"Id.int":              "记录ID不能为空.",
		"Title.required":      "文章标题不能为空.", // 文章标题
		"Cover.required":      "文章封面不能为空.", // 文章封面
		"Tags.required":       "文章标签不能为空.", // 文章标签
		"ItemId.int":          "站点ID不能为空.", // 站点ID
		"CateId.int":          "栏目ID不能为空.", // 栏目ID
		"IsTop.int":           "请选择是否置顶.",  // 是否置顶：1已置顶 2未置顶
		"ViewNum.int":         "浏览次数不能为空.", // 浏览次数
		"Author.required":     "作者不能为空.",   // 作者
		"SourceName.required": "来源名称不能为空.", // 来源名称
		"SourceUrl.required":  "来源链接不能为空.", // 来源链接
		"Guide.required":      "文章导读不能为空.", // 文章导读
		"Imgs.required":       "图集不能为空.",   // 图集
		"Content.required":    "文章内容不能为空.", // 文章内容
		"Status.int":          "请选择状态.",    // 状态：1显示 2隐藏
	}
}

// 编辑文章
type ArticleUpdateReq struct {
	Id         int    `form:"id" validate:"int"`
	Title      string `form:"title" validate:"required"`      // 文章标题
	Cover      string `form:"cover" validate:"required"`      // 文章封面
	Tags       string `form:"tags" validate:"required"`       // 文章标签
	ItemId     int    `form:"itemId" validate:"int"`          // 站点ID
	CateId     int    `form:"cateId" validate:"int"`          // 栏目ID
	IsTop      int    `form:"isTop" validate:"int"`           // 是否置顶：1已置顶 2未置顶
	ViewNum    int    `form:"viewNum" validate:"int"`         // 浏览次数
	Author     string `form:"author" validate:"required"`     // 作者
	SourceName string `form:"sourceName" validate:"required"` // 来源名称
	SourceUrl  string `form:"sourceUrl" validate:"required"`  // 来源链接
	Guide      string `form:"guide" validate:"required"`      // 文章导读
	Imgs       string `form:"imgs" validate:"required"`       // 图集
	Content    string `form:"content" validate:"required"`    // 文章内容
	Status     int    `form:"status" validate:"int"`          // 状态：1显示 2隐藏
	File       string `form:"file"`                           // 文件
}

// 更新表单验证
func (v ArticleUpdateReq) Messages() map[string]string {
	return validate.MS{
		"Id.int":              "记录ID不能为空.",
		"Title.required":      "文章标题不能为空.", // 文章标题
		"Cover.required":      "文章封面不能为空.", // 文章封面
		"Tags.required":       "文章标签不能为空.", // 文章标签
		"ItemId.int":          "站点ID不能为空.", // 站点ID
		"CateId.int":          "栏目ID不能为空.", // 栏目ID
		"IsTop.int":           "请选择是否置顶.",  // 是否置顶：1已置顶 2未置顶
		"ViewNum.int":         "浏览次数不能为空.", // 浏览次数
		"Author.required":     "作者不能为空.",   // 作者
		"SourceName.required": "来源名称不能为空.", // 来源名称
		"SourceUrl.required":  "来源链接不能为空.", // 来源链接
		"Guide.required":      "文章导读不能为空.", // 文章导读
		"Imgs.required":       "图集不能为空.",   // 图集
		"Content.required":    "文章内容不能为空.", // 文章内容
		"Status.int":          "请选择状态.",    // 状态：1显示 2隐藏
	}
}

// 设置是否置顶
type ArticleIsTopReq struct {
	Id    int `form:"id" validate:"int"`
	IsTop int `form:"isTop" validate:"int"`
}

// 设置状态参数验证
func (v ArticleIsTopReq) Messages() map[string]string {
	return validate.MS{
		"Id.int":    "记录ID不能为空.",
		"IsTop.int": "请选择是否置顶：1已置顶 2未置顶.",
	}
}

// 设置状态
type ArticleStatusReq struct {
	Id     int `form:"id" validate:"int"`
	Status int `form:"status" validate:"int"`
}

// 设置状态参数验证
func (v ArticleStatusReq) Messages() map[string]string {
	return validate.MS{
		"Id.int":     "记录ID不能为空.",
		"Status.int": "请选择状态：1显示 2隐藏.",
	}
}
